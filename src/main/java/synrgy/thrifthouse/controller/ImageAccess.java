package synrgy.thrifthouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.service.ImageService;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ImageAccess {

    @Autowired
    private ImageService imageService;

    @GetMapping(value = "/images/**", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> getFile(HttpServletRequest req) {
        String filename = req.getServletPath();
        String replaceAll = filename.replaceAll("/images/", "");
        Resource file = imageService.loadImage(replaceAll);
        return ResponseEntity.ok().body(file);
    }

}