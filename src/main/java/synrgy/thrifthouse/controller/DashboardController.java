package synrgy.thrifthouse.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.dashboard.DashboardSummaryDto;
import synrgy.thrifthouse.dto.dashboard.TransactionDetailDto;
import synrgy.thrifthouse.service.DashboardService;
import synrgy.thrifthouse.service.OrderService;
import synrgy.thrifthouse.service.UserService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/seller")
public class DashboardController {
    
    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @ApiOperation(value = "For: 'Dashboard Seller - Transaksi'")
    @GetMapping("/{user_id}/summary")
    public ResponseEntity<ResponseDto<DashboardSummaryDto>> getListOfTransactions(
        @PathVariable UUID user_id,
        @RequestParam(defaultValue = "0") Integer page,
        @RequestParam(defaultValue = "6") Integer size,
        @ApiParam(value = "DEFAULT VALUE: semua.  Accepts Value: semua, dikemas, dikirim, selesai, dibatalkan") @RequestParam(defaultValue = "semua") String status,
        @ApiParam(value = "DEFAULT VALUE: dibuat_terbaru.  Accepts Value: dibuat_terbaru, dibuat_terlama, dibayar_terbaru, dibayar_terlama") @RequestParam(defaultValue = "dibuat_terbaru") String sort_order,
        @ApiParam(value = "OPTIONAL. id_transaksi = TH-XXXXXXXXXXXX") @RequestParam(required = false) String id_transaksi
    ) {
        try {
            DashboardSummaryDto result = this.dashboardService.getDashboardSummary(user_id, page, size, status, sort_order, id_transaksi);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    result,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("\nDashboardController: " + e);
            throw e;
        }
    }

    @GetMapping("/{store_id}/transaction/{transaction_id}")
    public ResponseEntity<ResponseDto<TransactionDetailDto>> getTransactionDetail(
        @PathVariable UUID store_id,
        @PathVariable UUID transaction_id
    ) {
        TransactionDetailDto result = this.dashboardService.getTransactionDetail(transaction_id);
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }

    @DeleteMapping("/transaction/{transaction_id}")
    public ResponseEntity<ResponseDto<Map<String, String>>> deleteTransaction(
        @PathVariable UUID transaction_id
    ) {
        try {
            Map<String, String> response = new HashMap<>();
            this.dashboardService.deleteTransaction(transaction_id);
            response.put("transaction_id", transaction_id.toString());
            return new ResponseEntity<>(
            new ResponseDto<>(
                response,
                200,
                "success"
            ),
            HttpStatus.OK
        );
        } catch (Exception e) {
            System.out.println("DashboardController: " + e);
            throw e;
        }
        
    }

    @ApiOperation(value = "For updating ")
    @PutMapping("/{user_id}/transaction/{transaction_id}")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> updateTransactionStatus(
        @PathVariable UUID user_id,
        @PathVariable UUID transaction_id,
        @ApiParam(value = "OPTIONAL. For updating 'Status pemesanan'. Accepts value: ditolak, terkonfirmasi.") @RequestParam(required = false) String order_status,
        @ApiParam(value = "OPTIONAL. For updating 'Status pengiriman'. Accepts value: dikemas, dikirim.") @RequestParam(required = false) String packaging_status
    ) {
        Map<String, UUID> responseMap = new HashMap<>();
        try {
            this.orderService.updateTransactionStatus(transaction_id, order_status, packaging_status);
            responseMap.put("transactionId", transaction_id);
            return new ResponseEntity<>(
                new ResponseDto<>(
                    responseMap,
                    200,
                    "success"
                ),
                HttpStatus.OK
            );
        } catch (Exception e) {
            System.out.println("DashboardController: " + e);
            throw e;
        }
    }

    @GetMapping("/store-owner/{store_id}")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> findStoreOwner(@PathVariable UUID store_id) {
        Map<String, UUID> result = new HashMap<>();
        result.put("user_id", this.userService.findStoreOwnerByStoreId(store_id));
        return new ResponseEntity<>(
            new ResponseDto<>(
                result,
                200,
                "success"
            ),
            HttpStatus.OK
        );
    }
    
    @PostMapping("/create-transactions")
    public void generateTransactionsForStores() {
        this.orderService.generateTransactionsForStores();
    }

    // @PostMapping("/create-transaction-for-testing-purpose/{user_id}")
    // public void generateTransactionsForOneStore(@PathVariable UUID user_id) {
    //     this.dashboardService.generateTransactionsForOneStore(user_id);
    // }

}
