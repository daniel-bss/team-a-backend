package synrgy.thrifthouse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.user.UserChangePasswordDto;
import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.dto.user.UserProfileRequestDto;
import synrgy.thrifthouse.service.UserProfileService;
import synrgy.thrifthouse.service.UserService;

import java.util.Map;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users")
public class UserProfileController {

    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private UserService userService;

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDto<UserDto>> getUser(@PathVariable UUID userId) {
        UserDto userDto = this.userService.getUserById(userId);
        ResponseDto<UserDto> responseDto = new ResponseDto<>(
                userDto,
                HttpStatus.OK.value(),
                "success"
        );
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @PutMapping("/{userId}/profile")
    public ResponseEntity<ResponseDto<UserDto>> updateProfile(@PathVariable UUID userId, @ModelAttribute UserProfileRequestDto userProfileRequestDto) {

        return new ResponseEntity<>(
                new ResponseDto<UserDto>(
                        this.userProfileService.updateProfile(userId, userProfileRequestDto),
                        200,
                        "success"
                ),
                HttpStatus.OK
        );
    }

    @PutMapping("/{userId}/profile/password")
    public ResponseEntity<ResponseDto<Map<String, UUID>>> changePasswordUser(
            @PathVariable UUID userId, @RequestBody UserChangePasswordDto userChangePasswordDto
            ) {

        return new ResponseEntity<>(
                new ResponseDto<>(
                        this.userProfileService.updatePassword(userId, userChangePasswordDto),
                        200,
                        "success"
                ),
                HttpStatus.OK
        );
    }


}
