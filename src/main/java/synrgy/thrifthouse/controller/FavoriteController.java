package synrgy.thrifthouse.controller;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import synrgy.thrifthouse.dto.ResponseDto;
import synrgy.thrifthouse.dto.product.FavStoreDto;
import synrgy.thrifthouse.dto.product.PaginationProductDto;
import synrgy.thrifthouse.dto.product.ProductFavoriteDto;
import synrgy.thrifthouse.dto.product.ProductListDto;
import synrgy.thrifthouse.dto.store.FavoriteStoreDto;
import synrgy.thrifthouse.dto.store.PaginationStoreDto;
import synrgy.thrifthouse.service.ProductService;
import synrgy.thrifthouse.service.StoreService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users")
public class FavoriteController {

    @Autowired
    private StoreService storeService;

    @Autowired
    private ProductService productService;

    @PostMapping("{userId}/stores/favorites")
    public ResponseEntity<ResponseDto<FavoriteStoreDto>> store(
            @PathVariable UUID userId, @RequestBody FavoriteStoreDto favoriteStoreDto
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.storeService.favoriteStore(userId, favoriteStoreDto), 201, "created"));
    }

    @GetMapping("{userId}/stores/favorites")
    public ResponseEntity<ResponseDto<PaginationStoreDto<List<FavStoreDto>>>> getAllFavoriteStore(
            @PathVariable UUID userId,
            @RequestParam(defaultValue = "") @ApiParam(value = "Page number") String search,
            @RequestParam(defaultValue = "0") @ApiParam(value = "Page number") Integer page,
            @RequestParam(defaultValue = "2") @ApiParam(value = "Page size") Integer size,
            @RequestParam(defaultValue = "terbaru_disimpan") @ApiParam(value = "SORT BY urutkan. Accepts value: terbaru_disimpan (DEFAULT), terakhir_disimpan, abjad_awal, abjad_akhir") String urutkan
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.storeService.getAllFavoriteStore(userId, search, urutkan, page, size), 200, "success"));
    }

    @GetMapping("{userId}/products/favorites")
    public ResponseEntity<ResponseDto<PaginationProductDto<List<ProductListDto>>>> getAllFavoriteProdutc(
            @PathVariable UUID userId,
            @RequestParam(defaultValue = "") @ApiParam(value = "Page number") String search,
            @RequestParam(defaultValue = "0") @ApiParam(value = "Page number") Integer page,
            @RequestParam(defaultValue = "2") @ApiParam(value = "Page size") Integer size,
            @RequestParam(defaultValue = "terbaru_disimpan") @ApiParam(value = "SORT BY urutkan. Accepts value: terbaru_disimpan (DEFAULT), terakhir_disimpan, abjad_awal, abjad_akhir") String urutkan
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.productService.getAllFavoriteProduct(userId, search, urutkan, page, size), 200, "success"));
    }

    @PostMapping("{userId}/products/favorites")
    public ResponseEntity<ResponseDto<ProductFavoriteDto>> store(
            @PathVariable UUID userId, @RequestBody ProductFavoriteDto productFavoriteDto
    ){
        return ResponseEntity.ok(new ResponseDto<>(this.productService.storeFavoriteProduct(userId, productFavoriteDto), 201, "created"));
    }
}
