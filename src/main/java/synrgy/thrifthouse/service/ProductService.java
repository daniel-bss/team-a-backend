package synrgy.thrifthouse.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import synrgy.thrifthouse.dto.product.*;

public interface ProductService {

  // List<ProductListDto> productList();
  ProductDetailDto findDetail(UUID productId) throws IOException;
  PaginationProductDto<List<ProductListDto>> getAll(Integer page, Integer size, String sortBy, ProductFilter productFilter);
  Map<String, Set<Object>> getFilters();
  // UUID productStore (UUID userId, UUID productId);

  ProductDto storeProduct(ProductRequestDto productRequestDto, UUID sellerId);
  ProductFavoriteDto deleteProduct(UUID id);
  ProductDto getById(UUID id);
  ProductDto updateProduct(UUID id, ProductRequestDto productRequestDto, int[] changePhoto);

  // Service Favorite Product
  PaginationProductDto<List<ProductListDto>> getAllFavoriteProduct(UUID userId, String Search, String urutkan, Integer page, Integer size);
  ProductFavoriteDto storeFavoriteProduct(UUID userId, ProductFavoriteDto productFavoriteDto);

}
