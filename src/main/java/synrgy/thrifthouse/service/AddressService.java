package synrgy.thrifthouse.service;
import synrgy.thrifthouse.dto.address.AddressResponseDto;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface AddressService {
    List<AddressResponseDto> getAll();
    List<AddressResponseDto> getAllByUserId(UUID userId);
    AddressResponseDto getByAddressId(UUID userId, UUID addressId);
    AddressResponseDto store(UUID userId, AddressResponseDto addressResponseDto);
    Map<String, UUID> deleteByAddressId(UUID userId, UUID addressId);
    AddressResponseDto updateAddress(UUID userId, UUID addressId,AddressResponseDto addressResponseDto);

}
