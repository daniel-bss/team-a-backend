package synrgy.thrifthouse.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import synrgy.thrifthouse.exception.BadRequestException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.model.PasswordReset;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.PasswordResetRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.PasswordResetService;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class PasswordResetServiceImpl implements PasswordResetService {

    private final PasswordResetRepository passwordResetRepository;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JavaMailSender emailSender;

    private final Environment env;

    @Autowired
    public PasswordResetServiceImpl(PasswordResetRepository passwordResetRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, JavaMailSender emailSender, Environment env) {
        this.passwordResetRepository = passwordResetRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.emailSender = emailSender;
        this.env = env;
    }

    @Override
    public void createPasswordReset(String email) {
        Optional<User> userOptional = this.userRepository.findByEmail(email);
        userOptional.orElseThrow(() -> new ResourceNotFoundException("User not found with email : " + email));
        userOptional.ifPresent(user -> {
            PasswordReset passwordReset = new PasswordReset();
            passwordReset.setEmail(user.getEmail());
            passwordReset.setExpiredAt(LocalDateTime.now().plusHours(1));
            passwordReset.setToken(UUID.randomUUID().toString());
            PasswordReset save = passwordResetRepository.save(passwordReset);
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("noreply@thrifthouse.com");
            message.setTo(user.getEmail());
            message.setSubject("Thrifthouse password reset");
            message.setText("To reset your password, click the link below.\n" +
                    env.getProperty("frontend.url") +"/password-reset?" +
                    "url=http://localhost:8080password-reset?token=" + save.getToken());
            emailSender.send(message);
        });
    }

    @Override
    public void resetPassword(String token, String password) {
        Optional<PasswordReset> passwordResetOptional = this.passwordResetRepository.findByToken(token);
        passwordResetOptional.orElseThrow(() -> new BadRequestException("Invalid token"));
        passwordResetOptional.ifPresent(passwordReset -> {
            if (passwordReset.getExpiredAt().isAfter(LocalDateTime.now())) {
                Optional<User> userOptional = this.userRepository.findByEmail(passwordReset.getEmail());
                userOptional.ifPresent(user -> {
                    user.setPassword(passwordEncoder.encode(password));
                    this.userRepository.save(user);
                    this.passwordResetRepository.delete(passwordReset);
                });
            } else {
                throw new BadRequestException("Password reset token expired");
            }
        });
    }
}
