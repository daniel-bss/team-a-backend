package synrgy.thrifthouse.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.exception.RegistrationException;
import synrgy.thrifthouse.exception.ResourceNotFoundException;
import synrgy.thrifthouse.exception.UserCredentialException;
import synrgy.thrifthouse.mapper.UserMapper;
import synrgy.thrifthouse.model.Store;
import synrgy.thrifthouse.model.User;
import synrgy.thrifthouse.repository.StoreRepository;
import synrgy.thrifthouse.repository.UserRepository;
import synrgy.thrifthouse.service.NotificationService;
import synrgy.thrifthouse.service.UserService;

@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final List<String> allowedRoles = Arrays.asList("ROLE_ADMIN", "ROLE_SELLER", "ROLE_USER");

    @Autowired
    private NotificationService notificationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username);
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode users = objectMapper.createObjectNode();

        if (!(userOptional.isPresent())) {
            log.error("User was not found!");
            throw new UsernameNotFoundException("[ERROR] User was not found!");
        } else {
            User user = userOptional.get();
            log.info("User \"{}\" was found!", username);

            users.put("id", user.getId().toString());
            users.put("username", user.getUsername());
            users.put("email", user.getEmail());
            users.put("phone", user.getPhone());
            users.put("profileImg", user.getProfileImg());

            if (user.getStore() != null && user.getRole().equals("ROLE_SELLER")) {
                users.put("store_id", user.getStore().getId().toString());
            }

            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(user.getRole()));

            List<String> credentials;
            try {
                credentials = Arrays.asList(
                        objectMapper.writeValueAsString(users),
                        user.getPassword()
                );
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e.getMessage());
            }

            final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
            notificationService.createNotification(
                    user.getId(),
                    NotificationDto.builder().title("Notifikasi Login Baru").type("info").body(
                            "ThriftHouse mendeteksi adanya percobaan login pada akunmu pada tanggal "+ LocalDateTime.now()
                                    .format(DateTimeFormatter.ofPattern("dd MMMM yyyy, HH:mm:ss")
                                            .withLocale(Locale.forLanguageTag("id-ID"))) +". ")
                            .image(baseUrl+"/images/info-notif.png").build()
            );

            return new org.springframework.security.core.userdetails.User(credentials.get(0), credentials.get(1), authorities);
        }
    }

    @Override
    public List<UserDto> getUsers() {
        List<User> users = this.userRepository.findAll();
        return users.stream().map(userMapper::userToUserDto).collect(Collectors.toList());
    }

    @Override
    public UserDto getUserById(UUID id) {
        Optional<User> userOptional =  this.userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new ResourceNotFoundException("User was not found");
        }
        return userMapper.userToUserDto(userOptional.get());
    }

    @Override
    public void insertUser(UserDto userDto) {
        Optional<User> userOptionalByUsername = this.userRepository.findByUsername(userDto.getUsername());
        Optional<User> userOptionalByEmail = this.userRepository.findByEmail(userDto.getEmail());

        // Username validation
        if (userOptionalByUsername.isPresent()) {
            throw new RegistrationException("Username already exists!");
        }
        
        // Email validation
        if (userOptionalByEmail.isPresent()) {
            throw new RegistrationException("Email already exists!");
        }
        
        // Allowed roles validation
        String userRole = userDto.getRole();
        if (!(allowedRoles.contains(userRole))) {
            throw new RegistrationException("Allowed roles are: ROLE_ADMIN, ROLE_SELLER, ROLE_USER");
        }

        this.userRepository.save(userMapper.userDtoToUser(userDto));
    }

    @Override
    public void updateUser(UserDto userDto, UUID id) {
        this.userRepository.save(userMapper.userDtoToUser(userDto));
    }

    @Override
    public void deleteUser(UUID id) {
        this.userRepository.deleteById(id);
    }

    @Override
    public UUID findStoreOwnerByStoreId(UUID storeId) {
        Optional<Store> storeOptional = this.storeRepository.findById(storeId);
        if (!storeOptional.isPresent()) { throw new ResourceNotFoundException("Store was not found"); }
        UUID userId = storeOptional.get().getUser().getId();
        return userId;
    }

    @Override
    public UUID findStoreByUserId(UUID userId) {
        Optional<User> userOptional = this.userRepository.findById(userId);
        if (!userOptional.isPresent()) { throw new ResourceNotFoundException("User was not found"); }

        User user = userOptional.get();
        if (!user.getRole().equals("ROLE_SELLER")) { throw new UserCredentialException("User is not a seller."); } 
        else {
            UUID storeId = user.getStore().getId();
            return storeId;
        }

    }

}
