package synrgy.thrifthouse.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import synrgy.thrifthouse.dto.review.ReviewList;
import synrgy.thrifthouse.model.Review;
import synrgy.thrifthouse.model.ReviewKey;

@Repository
public interface ReviewRepository extends JpaRepository<Review, ReviewKey> {
  Integer countByStoreId(UUID storeId);
  Page<Review> findByStoreId(Pageable pageable,UUID storeId);
  @Query(value = "SELECT r FROM Review r WHERE r.store.id=:storeId AND r.rating>=:rate AND r.rating<:rate+1")
  Page<Review> findByStoreId(Pageable pageable, UUID storeId, Integer rate);
  @Query(value = "SELECT r FROM Review r WHERE r.store.id=:storeId AND (r.photos is not null)")
  Page<Review> findByStoreIdAndPhotosNotNull(Pageable pageable,UUID storeId);
  @Query(value = "SELECT r FROM Review r WHERE r.store.id=:storeId AND (r.photos is null)")
  Page<Review> findByStoreIdAndPhotosNull(Pageable pageable,UUID storeId);
  @Query(value = "SELECT r FROM Review r WHERE r.store.id=:storeId AND (r.photos is not null) AND r.rating>=:rate AND r.rating<:rate+1")
  Page<Review> findByStoreIdAndPhotosNotNull(Pageable pageable,UUID storeId, Integer rate);
  @Query(value = "SELECT r FROM Review r WHERE r.store.id=:storeId AND (r.photos is null) AND r.rating>=:rate AND r.rating<:rate+1")
  Page<Review> findByStoreIdAndPhotosNull(Pageable pageable,UUID storeId, Integer rate);

  @Query(value = "select totalRate, totalSelling, totalReview, totalRated " +
          "from " +
          "(select r.store_id as storeId, avg(r.rating) as totalRate, count(r.description) as totalReview, count(r.rating) as totalRated " +
          "from review r where r.store_id=:storeId group by r.store_id) as r1 " +
          "join" +
          "(select p.store_id as storeId, count(case p.sold when true then 1 else null end) as totalSelling " +
          "from store s join product p on p.store_id = s.id where p.store_id=:storeId " +
          "group by p.store_id) as r2 " +
          "on r1.storeId = r2.storeId", nativeQuery = true)
  Optional<ReviewList> findInformation(UUID storeId);
}
