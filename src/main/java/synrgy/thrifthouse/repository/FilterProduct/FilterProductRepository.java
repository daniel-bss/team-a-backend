package synrgy.thrifthouse.repository.FilterProduct;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ClassUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import synrgy.thrifthouse.dto.product.ProductFilter;
import synrgy.thrifthouse.dto.product.ProductFilterListDto;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.repository.ProductRepository;

import javax.persistence.Lob;
import javax.persistence.OneToOne;
import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
@RequiredArgsConstructor
public class FilterProductRepository {
    private final ProductRepository repository;
    public List<Product> findAllByFilter(ProductFilter productFilter) throws IllegalAccessException {
        return findByQueryResult(createFilters(productFilter));
    }

    public List<Product> findByQueryResult(List<Filter> filters){
        if(filters.size()>0) {
            return repository.findAll(getSpecificationFromFilters(filters));
        }else {
            return repository.findAll();
        }
    }

    public Page<Product> findAllByFilter(ProductFilter productFilter, Pageable pageable) throws IllegalAccessException {
        return findByQueryResult(createFilters(productFilter), pageable);
    }

    public List<Filter> createFilters(ProductFilter productFilter) throws IllegalAccessException {
        Field[] fields = productFilter.getClass().getDeclaredFields();
        List<Filter> filters = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.get(productFilter)!=null) {
                Filter.FilterBuilder filter = Filter.builder();
                Class<?> type = field.getType();
                String o = field.get(productFilter).toString();
                System.out.println("whatisit?: "+ o);
                String value;
                Name annotation = field.getAnnotation(Name.class);
                if (annotation!=null) {
                    value = annotation.value();
                    if (value.isEmpty()) value = field.getName();
                } else {
                    value = field.getName();
                }
                filter.field(value);
                if (type.isAssignableFrom(List.class)) {
                    System.out.println("list: "+ o);
                    List<String> list = Arrays.asList(o.substring(1, o.length() - 1).split(", "));
                    filter.operator(QueryOperator.IN).values(list);
                } else if (value.contains("Up") || value.contains("Down")) {
                    if(value.contains("Up")) {
                        filter.field(value.substring(0,value.length()-"Up".length()));
                        filter.value(o);
                        filter.operator(QueryOperator.LESS_THAN);
                    } else {
                        filter.field(value.substring(0,value.length()-"Down".length()));
                        filter.value(o);
                        filter.operator(QueryOperator.GREATER_THAN);
                    }
                } else if (value.contains("search")) {
                    Pattern pattern = Pattern.compile("(.+)[:](.+)");
                    Matcher matcher = pattern.matcher(o);
                    String keyword;
                    if (matcher.find()) {
                        keyword = matcher.group(1);
                        String fieldNameFilter = matcher.group(2);
                        List<String> iteration = Arrays.asList(fieldNameFilter.split(","));
                        filter.values(iteration);
                    } else {
                        keyword = o;
                        List<String> iteration = Arrays.stream(Product.class.getDeclaredFields()).filter(field1 -> {
                            if (field1.getAnnotation(Filtered.class)!=null) {
                                Filtered filtered = field1.getAnnotation(Filtered.class);
                                return filtered.required();
                            } else return (field1.getType().isAssignableFrom(String.class)
                                    && field1.getAnnotation(Lob.class) == null);
                        }).map(field2 -> {
                            if (field2.getAnnotation(Filtered.class)!=null) {
                                List<String> check = Arrays.asList(field2.getAnnotation(Filtered.class).joinField()
                                        .split(","));
                                check.forEach(s -> {
                                    try {
                                        if (field2.getClass().getField(s).getAnnotation(Lob.class)!=null)
                                            throw new IllegalArgumentException("This field type not supported yet.");
                                    } catch (NoSuchFieldException e) {
                                        // do nothing
                                    }
                                });
                                return field2.getName()+"."+field2.getAnnotation(Filtered.class).joinField();
                            }
                            return field2.getName();
                        }).collect(Collectors.toList());
                        filter.values(iteration);
                        System.out.println(iteration);
                    }
                    filter.value(keyword)
                            .operator(QueryOperator.LIKE);
                }else {
                    System.out.println("string: "+ o);
                    filter.value(o);
                    filter.operator(QueryOperator.EQUALS);
                    if (value.contains(".")) {
                        filter.joinField(value.split("\\.")[0]);
                        filter.field(value.split("\\.")[1]);
                    }
                }
                if (type.isAssignableFrom(List.class)) {
                    String s = o.substring(1, o.length() - 1);
                    if (s.length() > 0) {
                        System.out.println("masuk sini:list tidak kosong\nukuran listnya:" + s.length());
                        filters.add(filter.build());
                    }
                } else {
                    if (o.length() > 0) {
                        System.out.println("masuk sini:string tidak kosong\nukuran stringnya:" + o.length());
                        filters.add(filter.build());
                    }
                }
            }
        }
        return filters;
    }

    public Page<Product> findByQueryResult(List<Filter> filters, Pageable pageable){
        if(filters.size()>0) {
            return repository.findAll(getSpecificationFromFilters(filters), pageable);
        }else {
            return repository.findAll(pageable);
        }
    }

    public Map<String, Set<Object>> getFilters() {
        List<ProductFilterListDto> all = repository.findFilters();

        Map<String, Set<Object>> filters = new HashMap<>();

        Set<String> brands = all.stream().map(ProductFilterListDto::getBrand).collect(Collectors.toSet());
        Set<String> sizes = all.stream().map(ProductFilterListDto::getSize).collect(Collectors.toSet());
        Set<String> conditions = all.stream().map(ProductFilterListDto::getCondition).collect(Collectors.toSet());

        filters.put("brands", Collections.singleton(brands.stream().sorted()));
        filters.put("sizes", Collections.singleton(sizes.stream().sorted()));
        filters.put("conditions", Collections.singleton(conditions.stream().sorted()));

        return filters;
    }

    private Specification<Product> getSpecificationFromFilters(List<Filter> filters) {
        Specification<Product> specification = null;
        for (Filter input : filters) {
            if(input.getField().equals("search")) {
                List<Filter> subFilter = createFiltersFromFilter(input);
                for (Filter filter: subFilter) {
                    if (specification==null) {
                        specification = where(createSpecification(filter));
                    } else {
                        specification = specification.or(createSpecification(filter));
                    }
                }
            } else {
                if (specification==null) {
                    specification = where(createSpecification(input));
                } else {
                    specification = specification.and(createSpecification(input));
                }
            }
        }
        return specification;
    }

    private List<Filter> createFiltersFromFilter(Filter searchFilter) {
        List<Filter> filters = new ArrayList<>();
        for(String field: searchFilter.getValues()) {
            if (field.contains(".")) {
                List<Filter> filtersFromJoin = createFiltersFromFilter(
                        Filter.builder().field(field.split("\\.")[0])
                                .values(Arrays.asList(field.split("\\.")[1].split(",")))
                                .value(searchFilter.getValue())
                                .operator(searchFilter.getOperator())
                                .joinField(field.split("\\.")[0])
                                .build()
                );
                filters.addAll(filtersFromJoin);
            } else {
                Filter filter = Filter.builder().field(field)
                        .value(searchFilter.getValue())
                        .operator(searchFilter.getOperator())
                        .joinField(searchFilter.getJoinField())
                        .build();
                filters.add(filter);
            }
        }
        return filters;
    }

    private Specification<Product> createSpecification(Filter input) {
        switch (input.getOperator()){
            case BETWEEN:
                if (input.getValues().size() > 2) throw new IllegalArgumentException("To check between value, should have only 2 values");
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.between(root.get(input.getField()),
                                input.getValues().get(0), input.getValues().get(1));
            case EQUALS:
                return (root, query, criteriaBuilder) -> {
                    if (input.getJoinField()!=null) {
                        return criteriaBuilder.equal(root.join(input.getJoinField()).get(input.getField()),
                                castToRequiredType(root.get(input.getField()).getJavaType(), input.getValue()));
                    }
                    return criteriaBuilder.equal(root.get(input.getField()),
                            castToRequiredType(root.get(input.getField()).getJavaType(), input.getValue()));
                };
            case NOT_EQ:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.notEqual(root.get(input.getField()),
                                castToRequiredType(root.get(input.getField()).getJavaType(), input.getValue()));
            case GREATER_THAN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.gt(root.get(input.getField()),
                                (Number) castToRequiredType(root.get(input.getField()).getJavaType(), input.getValue()));
            case LESS_THAN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.lt(root.get(input.getField()),
                                (Number) castToRequiredType(root.get(input.getField()).getJavaType(), input.getValue()));
            case LIKE:
                return (root, query, criteriaBuilder) -> {
                    if (input.getJoinField()!=null) {
                        return criteriaBuilder.like(criteriaBuilder.lower(root.join(input.getJoinField()).get(input.getField())), ("%"+(input.getValue().toLowerCase(Locale.ROOT))+"%"));
                    }
                    return criteriaBuilder.like(criteriaBuilder.lower(root.get(input.getField())), ("%"+(input.getValue().toLowerCase(Locale.ROOT))+"%"));
                };
            case IN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.in(root.get(input.getField()))
                                .value(input.getValues());
            default:
                throw new RuntimeException("Operation not supported yet");
        }
    }

    private Object castToRequiredType(Class fieldType, String value) {
        if(fieldType.isAssignableFrom(Integer.class)){
            System.out.println("masuk sini integer " + fieldType);
            return Integer.valueOf(value);
        }else if(fieldType.isAssignableFrom(Double.class)){
            System.out.println("masuk sini double " + fieldType);
            return Double.valueOf(value);
        }else if(Enum.class.isAssignableFrom(fieldType)){
            System.out.println("masuk sini enum " + fieldType);
            return Enum.valueOf(fieldType, value);
        }else if(Long.class.isAssignableFrom(fieldType)) {
            System.out.println("masuk sini long " + fieldType);
            return Long.valueOf(value);
        }else if(UUID.class.isAssignableFrom(fieldType)){
            System.out.println("masuk sini uuid " + fieldType);
            return UUID.fromString(value);
        } else if(ClassUtils.isAssignable(fieldType, Boolean.class)) {
            System.out.println("masuk sini boolean " + fieldType);
            return Boolean.parseBoolean(value);
        }else if(String.class.isAssignableFrom(fieldType)) {
            System.out.println("masuk sini string " + fieldType);
            return String.valueOf(value);
        }
        System.out.println("masuk sini null " + fieldType);
        return null;
    }

    private Object castToRequiredType(Class fieldType, List<String> value) {
        List<Object> lists = new ArrayList<>();
        for (String s : value) {
            lists.add(castToRequiredType(fieldType, s));
        }
        return lists;
    }

    public enum QueryOperator {
        BETWEEN,
        GREATER_THAN,
        LESS_THAN,
        EQUALS,
        LIKE,
        NOT_EQ,
        IN
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Documented
    public @interface Name {
        String value() default "";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @Documented
    public @interface Filtered {
        boolean required() default true;
        String joinField() default "";
    }
}
