package synrgy.thrifthouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import synrgy.thrifthouse.model.FavoriteProduct;
import synrgy.thrifthouse.model.FavoriteProductKey;

public interface FavoriteProductRepository extends JpaRepository<FavoriteProduct, FavoriteProductKey> {

}
