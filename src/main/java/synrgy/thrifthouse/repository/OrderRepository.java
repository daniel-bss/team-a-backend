package synrgy.thrifthouse.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import synrgy.thrifthouse.dto.dashboard.TransactionDetailDto;
import synrgy.thrifthouse.model.Order;

@Transactional
public interface OrderRepository extends JpaRepository<Order, UUID> {
    List<Order> findByUser_IdAndPackagingStatusInIgnoreCaseAllIgnoreCaseOrderByExpiredAtDesc(UUID id, Collection<String> packagingStatuses);

    List<Order> findByUser_IdOrderByExpiredAtDesc(UUID id);

    List<Order> findByUser_IdAndStatusAllIgnoreCaseOrderByExpiredAtDesc(UUID id, String status);

    List<Order> findByUser_IdAndPackagingStatusAllIgnoreCaseOrderByExpiredAtDesc(UUID id, String packagingStatus);

    List<Order> findByUser_IdAndIsRejectedOrderByExpiredAtDesc(UUID id, Boolean isRejected);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Order o SET o.receipt = ?2 WHERE o.id = ?1")
    Integer putReceipt(UUID id, String receipt);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Order o SET o.status = ?2 WHERE o.id = ?1")
    Integer putStatus(UUID id, String status);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Order o SET o.isRejected = true WHERE o.id = ?1")
    Integer putIsRejectedToTrue(UUID id);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Order o SET o.packagingStatus = ?2 WHERE o.id = ?1")
    Integer putPackagingStatus(UUID id, String status);

    @Query(value = 
    "SELECT new synrgy.thrifthouse.dto.dashboard.TransactionDetailDto(o.id, u.username, u.id, o.orderCode, o.expiredAt, o.productsPrice, u.phone, o.status, o.packagingStatus, o.receipt, o.deliveryService, o.shippingCost, o.shippingService, o.estimatedTimeOfDeparture, a.fullAddress, a.province, a.city, a.district, a.village, a.postalCode) " +
    "FROM Order o " +
    "JOIN User u " +
    "ON o.user = u.id " +
    "JOIN Address a " +
    "ON a.id = o.address " +
    "WHERE o.id = ?1 ")
    TransactionDetailDto getTransactionDetail(UUID orderId);

    Optional<Order> findByIdAndUser_Id(UUID id, UUID id1);
    
}
