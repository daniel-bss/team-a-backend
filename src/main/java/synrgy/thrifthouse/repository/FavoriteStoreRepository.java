package synrgy.thrifthouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import synrgy.thrifthouse.model.FavoriteStore;
import synrgy.thrifthouse.model.FavoriteStoreKey;

@Repository
public interface FavoriteStoreRepository extends JpaRepository<FavoriteStore, FavoriteStoreKey> {

}
