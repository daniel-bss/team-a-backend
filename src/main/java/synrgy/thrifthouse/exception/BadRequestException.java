package synrgy.thrifthouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import synrgy.thrifthouse.dto.ResponseDto;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  private final ResponseDto<Object> responseDto;

  public BadRequestException(ResponseDto<Object> responseDto) {
    super();
    this.responseDto = responseDto;
  }

  public BadRequestException(String message) {
    super(message);
    this.responseDto = new ResponseDto<>();
    this.responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
    this.responseDto.setMessage(message);
  }

  public BadRequestException(String message, Throwable cause) {
    super(message, cause);
    this.responseDto = new ResponseDto<>();
    this.responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
    this.responseDto.setMessage(message);
  }

  public ResponseDto<Object> getResponseDto() {
    return responseDto;
  }
}
