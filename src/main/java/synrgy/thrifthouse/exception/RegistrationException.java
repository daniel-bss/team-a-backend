package synrgy.thrifthouse.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import synrgy.thrifthouse.dto.ResponseDto;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RegistrationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final ResponseDto<Object> responseDto;

    public RegistrationException(String message) {
        super(message);
        this.responseDto = new ResponseDto<>();
        this.responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
        this.responseDto.setMessage(message);
    }
    
}