package synrgy.thrifthouse.dto.review;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewPostDto {
    private UUID userId;
    private String description;
    private Integer rating = 0;

    @Setter(lombok.AccessLevel.NONE)
    @Getter(lombok.AccessLevel.NONE)
    private Boolean isAnonim = false;

    @JsonProperty("isAnonim")
    public void setIsAnonim(String isAnonim) {
        this.isAnonim = Boolean.parseBoolean(isAnonim);
    }

    @JsonProperty("isAnonim")
    public boolean isAnonim() {
        return this.isAnonim;
    }
}
