package synrgy.thrifthouse.dto.order;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertOrderDtoNew {
    
    private UUID userId;
    private UUID addressId;
    private UUID bankId;
    private List<StoreTransaction> storeTransactions;

}
