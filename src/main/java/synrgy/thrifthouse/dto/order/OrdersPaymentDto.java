package synrgy.thrifthouse.dto.order;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import synrgy.thrifthouse.dto.bank.BankDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrdersPaymentDto {
    
    private UUID order_id;
    private UUID user_id;
    private UUID store_id;
    private String orderCode;
    private BankDto bank;
    private String status;
    private String packagingStatus;
    private String expiredAt;
    private Integer productsPrice;
    private String deliveryService;
    private Integer shippingCost;
    private String shippingService;
    private String estimatedTimeOfDeparture;
    private String receipt;
    private List<UUID> productIds;

}
