package synrgy.thrifthouse.dto.order;

import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreTransaction {

    private List<UUID> productIds;
    private String deliveryService;
    private Integer shippingCost;
    private String shippingService;
    private String estimatedTimeOfDeparture;

}
