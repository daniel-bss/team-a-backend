package synrgy.thrifthouse.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequestDto {

    private String name;

    private String brand;

    private String size;

    private String condition;

    private Integer price;

    private Integer height;

    private Integer width;

    private Integer weight;

    private String material;

    private String description;

    private MultipartFile[] photos;

    private String category;

    private String subcategory1;

    private String subcategory2;
}
