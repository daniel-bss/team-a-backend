package synrgy.thrifthouse.dto.product;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import synrgy.thrifthouse.repository.FilterProduct.FilterProductRepository;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductFilter {
    @ApiParam(value = "Search keyword, format: keyword|field1,field2,relation.field1,...(without space) or keyword")
    private String search;

    @ApiParam(value = "Category name, accept multiple value with format: name1,name2,name3,...(without space)")
    private List<String> category;

    @ApiParam(value = "Subcategory1 name, accept multiple value with format: name1,name2,name3,...(without space)")
    private List<String> subcategory1;

    @ApiParam(value = "Subcategory2 name, accept multiple value with format: name1,name2,name3,...(without space)")
    private List<String> subcategory2;

    @ApiParam(value = "Brand name, accept multiple value with format: name1,name2,name3,...(without space)")
    private List<String> brand;

    @ApiParam(value = "Size product, accept multiple value with format: size1,size2,size3,...(without space)")
    @FilterProductRepository.Name("size")
    private List<String> sizeProduct;

    @ApiParam(value = "Condition product, accept multiple value with format: condition1,condition2,condition3,...(without space)")
    private List<String> condition;

    @ApiParam(value = "Status sold, accept value: true,false")
    private Boolean sold;

    @ApiParam(value = "Max value price")
    private Integer priceUp;

    @ApiParam(value = "Min value price")
    private Integer priceDown;

    @ApiParam(value = "Id Store")
    @FilterProductRepository.Name("store.id")
    private UUID storeId;
}
