package synrgy.thrifthouse.dto.storeSettings;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreBankDto {
    
    private String bankName;
    private String bankNumber;
    private String bankHolder;

}
