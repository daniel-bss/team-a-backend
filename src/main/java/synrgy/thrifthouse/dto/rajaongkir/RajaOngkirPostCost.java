package synrgy.thrifthouse.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RajaOngkirPostCost {
    private String origin;
    private String destination;
    private String weight;
    private String courier;
}
