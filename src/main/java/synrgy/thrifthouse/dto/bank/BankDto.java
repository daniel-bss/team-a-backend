package synrgy.thrifthouse.dto.bank;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankDto {

    private UUID id;
    private String bankName;
    private String bankNumber;
    private String bankHolder;
    private String bankLogo;

}
