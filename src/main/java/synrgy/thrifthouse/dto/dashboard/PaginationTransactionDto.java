package synrgy.thrifthouse.dto.dashboard;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaginationTransactionDto {

    private Long totalItems;
    private Integer totalPages;
    private Integer currentPage;
    private List<TransactionDto> transactions;

}
