package synrgy.thrifthouse.dto.dashboard;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardSummaryDto {
    
    private UUID storeId;
    private StoreSummaryDto storeSummary;
    private PaginationTransactionDto transactionDetails;

}
