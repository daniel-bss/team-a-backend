package synrgy.thrifthouse.dto.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreSummaryDto {
    
    Integer totalProduct;
    Integer revenue;
    Integer transaction;
    Double averageReview;
    Integer favoriteStore;

}
