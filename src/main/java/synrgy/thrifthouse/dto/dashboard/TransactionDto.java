package synrgy.thrifthouse.dto.dashboard;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class TransactionDto {
    
    private UUID orderId;
    private String kodeTransaksi;
    private Integer price;
    private String buyerName;
    private String orderedAt;
    // @JsonIgnore
    private Date orderedAt_DateType;
    private String status;
    private String packagingStatus;
    private String receipt;
    @JsonIgnore
    private String receiptIsPut = "false";

    public TransactionDto(UUID orderId, String kodeTransaksi, Integer price, String buyerName, String orderedAt,
            Date orderedAt_DateType, String status, String packagingStatus, String receipt) {
        this.orderId = orderId;
        this.kodeTransaksi = kodeTransaksi;
        this.price = price;
        this.buyerName = buyerName;
        this.orderedAt = orderedAt;
        this.orderedAt_DateType = orderedAt_DateType;
        this.status = status;
        this.packagingStatus = packagingStatus;
        this.receipt = receipt;
        if (this.receipt != null) {
            this.receiptIsPut = (this.receipt.length() > 0) ? "true" : "false";
        } else {
            this.receiptIsPut = "false";
        }
    }
    
}
