package synrgy.thrifthouse.dto.store;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreKtpDto {
    private String ktpImg;
    private String ktpNumber;
}
