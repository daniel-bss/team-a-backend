package synrgy.thrifthouse.dto.user;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SellerAccountDto2 {
    
    private UUID user_id;
    private UUID store_id;
    private String fullname;
    private String email;
    private String phone;
    private String birth;
    private String gender;
    private String ktpNumber;
    private String ktpImg;

}
