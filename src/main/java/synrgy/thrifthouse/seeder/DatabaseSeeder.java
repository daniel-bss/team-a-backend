package synrgy.thrifthouse.seeder;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.github.javafaker.Faker;

import synrgy.thrifthouse.model.*;
import synrgy.thrifthouse.repository.*;

@Component
public class DatabaseSeeder {
  private final ProductRepository productRepository;
  private final StoreRepository storeRepository;
  private final UserRepository userRepository;
  private final ReviewRepository reviewRepository;
  private final AddressRepository addressRepository;
  private final OrderRepository orderRepository;
  private final BankRepository bankRepository;
  private final FavoriteProductRepository favoriteProductRepository;
  private final FavoriteStoreRepository favoriteStoreRepository;
  private final JdbcTemplate jdbcTemplate;

  private final Faker faker = new Faker(Locale.forLanguageTag("id-ID"));

  private final String[] roles = { "ROLE_ADMIN", "ROLE_USER", "ROLE_SELLER" };

  @Autowired
  public DatabaseSeeder(
          ProductRepository productRepository,
          StoreRepository storeRepository,
          UserRepository userRepository,
          ReviewRepository reviewRepository,
          AddressRepository addressRepository,
          OrderRepository orderRepository,
          BankRepository bankRepository,
          JdbcTemplate jdbcTemplate,
          FavoriteProductRepository favoriteProductRepository,
          FavoriteStoreRepository favoriteStoreRepository) {
    this.productRepository = productRepository;
    this.storeRepository = storeRepository;
    this.userRepository = userRepository;
    this.reviewRepository = reviewRepository;
    this.addressRepository = addressRepository;
    this.jdbcTemplate = jdbcTemplate;
    this.bankRepository = bankRepository;
    this.orderRepository = orderRepository;
    this.favoriteProductRepository = favoriteProductRepository;
    this.favoriteStoreRepository = favoriteStoreRepository;
  }

  @EventListener
  public void seed(ContextRefreshedEvent event) {
    // seedUsers();
    // seedAddress();
    // seedStore();
    // seedProduct();
    // seedReview();
    // seedFavorite();
    // seedFavoriteProducts();
    // seedCart();
    // seedBank();
    // seedOrder();
  }

  private void seedCart() {
    String sql = "SELECT count(U.*) FROM cart U";
    Integer u = jdbcTemplate.queryForObject(sql, Integer.class);
    if (u != null) {
      if(u > 0) return;
    }

    List<User> users = userRepository.findByRole("ROLE_USER");
    List<Product> products = productRepository.findAll();
    users = users.stream().peek((user) -> {
      List<Product> product = new ArrayList<>();
      for (int i = 0; i < 10; i++) {
        product.add(products.get(faker.number().numberBetween(0,products.size()-1)));
      }
      user.setCart(product);
    }).collect(Collectors.toList());
    userRepository.saveAll(users);
  }

  private void seedFavorite() {
    String sql = "SELECT count(U.*) FROM favorite U";
    Integer u = jdbcTemplate.queryForObject(sql, Integer.class);
    if (u != null) {
      if(u > 0) return;
    }

    List<User> users = userRepository.findAll();
    List<Store> stores = storeRepository.findAll();

    Random rand;
    for (User user: users) {
        rand = new Random();
        List<FavoriteStore> favorites = new ArrayList<>();
        int num1 = rand.nextInt(10) + 1;
        List<Store> selectedStores = new ArrayList<>();
        for (int i = 0; i < num1; i++) {
            Store store = stores.get(rand.nextInt(stores.size()));

            if (!selectedStores.contains(store)) {
                selectedStores.add(store);
            }
        }

        for (Store store : selectedStores) {
            FavoriteStore favStore = new FavoriteStore(
                new FavoriteStoreKey(user.getId(), store.getId()),
                user,
                store
            );
            this.favoriteStoreRepository.save(favStore);
            favorites.add(favStore);
        }
        user.setStores(favorites);
        userRepository.save(user);

    }  
  }

  private void seedReview() {
    long u = reviewRepository.count();
    if(u > 0) return;
    List<User> users = userRepository.findAll();
    List<Store> stores = storeRepository.findAll();

    for (Store store : stores) {
      for (int i = 0; i < faker.number().numberBetween(10, 30); i++) {
        Review review = new Review();
        User user = users.get(faker.number().numberBetween(0, users.size()-1));
        review.setId(new ReviewKey(user.getId(), store.getId()));
        review.setUser(user);
        review.setStore(store);
        review.setRating(faker.number().numberBetween(1,5));
        review.setDescription(faker.number().numberBetween(0,1)!=0 ? faker.lorem().sentence() : null);
        List<String> arr = new ArrayList<>(Arrays.asList("https://source.unsplash.com/480x480?clothes",
                "https://source.unsplash.com/480x480?shirt", "https://source.unsplash.com/480x480?shoes",
                "https://source.unsplash.com/480x480?hat"));
        review.setPhotos(arr);
        review.setCreatedAt(LocalDateTime.now());
        List<String> userIds = new ArrayList<>();
        for (int j = 0; j < 3; j++) {
          userIds.add(users.get(faker.number().numberBetween(0,users.size()-1)).getId().toString());
        }
        review.setUserIds(userIds);
        reviewRepository.save(review);
      }
    }
  }

  private void seedProduct() {
    long u = productRepository.count();
    if(u > 0) return;
    String BASE_URL = "https://thrifthouse.herokuapp.com/images";
    Random rand = new Random();
    List<Store> stores = storeRepository.findAll();
    for (Store store : stores) {
      List<Product> products = new ArrayList<>();
      for (int j = 0; j < faker.number().numberBetween(10, 25); j++) {
        Product product = new Product();
        product.setId(UUID.randomUUID()); // 10
        product.setBrand(faker.options().option("Uniqlo", "Adidas", "Nike", "Kenzo", "Pull&Bear", "Dior"));
        product.setSize(faker.options().option("S", "M", "L", "XL"));
        product.setCondition(faker.options().option("Baru dengan Tag", "Seperti Baru", "Baik", "Cukup"));
        product.setPrice((rand.nextInt(1500 - 350 + 1) + 350) * 100);
        product.setHeight(faker.number().numberBetween(10, 50));
        product.setWidth(faker.number().numberBetween(10, 50));
        product.setWeight(faker.number().numberBetween(100, 1001));
        product.setMaterial(faker.options().option("Katun", "Kulit", "Bordir", "Lainnya"));
        product.setDescription(faker.lorem().paragraph());
        String productName = faker.options().option(
          "Printed Turquoise Pullover Hoodie", "Grey Sport Hoodie",
          "Brown Sneakers Blast", "Blue Presto Shoes", "Air Max 97 Shoes", "Black Running Shoes",
          "Airism Polo Navy", "Sleeved T-Shirt", "K-Tiger T-shirt Black", "XDYE Splash T-Shirt"
        );
        product.setName(productName);
        List<String> arr = null;
        switch (productName) {
          case "Printed Turquoise Pullover Hoodie":
            arr = new ArrayList<>(Arrays.asList("/hoodies/hoodie1a.png", "/hoodies/hoodie1b.png", "/hoodies/hoodie1c.png", "/hoodies/hoodie1d.png"));
            break;
          case "Grey Sport Hoodie":
            arr = new ArrayList<>(Arrays.asList("/hoodies/hoodie2a.png", "/hoodies/hoodie2b.png", "/hoodies/hoodie2c.png", "/hoodies/hoodie2d.png"));
            break;
          case "Brown Sneakers Blast":
            arr = new ArrayList<>(Arrays.asList("/shoes/shoes1a.png", "/shoes/shoes1b.png", "/shoes/shoes1c.png", "/shoes/shoes1d.png"));
            break;
          case "Blue Presto Shoes":
            arr = new ArrayList<>(Arrays.asList("/shoes/shoes2a.png", "/shoes/shoes2b.png", "/shoes/shoes2c.png", "/shoes/shoes2d.png"));
            break;
          case "Air Max 97 Shoes":
            arr = new ArrayList<>(Arrays.asList("/shoes/shoes3a.png", "/shoes/shoes3b.png", "/shoes/shoes3c.png", "/shoes/shoes3d.png"));
            break;
          case "Black Running Shoes":
            arr = new ArrayList<>(Arrays.asList("/shoes/shoes4a.png", "/shoes/shoes4b.png", "/shoes/shoes4c.png", "/shoes/shoes4d.png"));
            break;
            case "Airism Polo Navy":
            arr = new ArrayList<>(Arrays.asList("/tshirts/tshirt1a.png", "/tshirts/tshirt1b.png", "/tshirts/tshirt1c.png", "/tshirts/tshirt1d.png"));
            break;
          case "Sleeved T-Shirt":
            arr = new ArrayList<>(Arrays.asList("/tshirts/tshirt2a.png", "/tshirts/tshirt2b.png", "/tshirts/tshirt2c.png", "/tshirts/tshirt2d.png"));
            break;
          case "K-Tiger T-shirt Black":
            arr = new ArrayList<>(Arrays.asList("/tshirts/tshirt3a.png", "/tshirts/tshirt3b.png", "/tshirts/tshirt3c.png", "/tshirts/tshirt3d.png"));
            break;
          case "XDYE Splash T-Shirt":
            arr = new ArrayList<>(Arrays.asList("/tshirts/tshirt4a.png", "/tshirts/tshirt4b.png", "/tshirts/tshirt4c.png", "/tshirts/tshirt4d.png"));
            break;
          default:
            arr = new ArrayList<>(Arrays.asList("https://source.unsplash.com/480x480?clothes",
                "https://source.unsplash.com/480x480?shirt", "https://source.unsplash.com/480x480?shoes",
                "https://source.unsplash.com/480x480?hat"));
        }

        for (int i = 0; i < arr.size(); i++) {
          String photo = arr.get(i);
          if (!photo.toLowerCase().contains("source.unsplash")) {
            photo = BASE_URL + photo;
            arr.set(i, photo);
          }
        }
        
        product.setPhotos(arr);

        Map<String, List<String>> typePria = new HashMap<>();
        typePria.put("aksesoris", Arrays.asList("ikatpinggang", "jam", "kacamata", "topi"));
        typePria.put("sepatu", Arrays.asList("boots", "formal", "sendal", "slipon", "sneakers", "olahraga"));
        typePria.put("luaran", Arrays.asList("crewneck", "hoodie", "jaket", "jas", "mantel", "sweater"));
        typePria.put("bawahan", Arrays.asList("celanapendek", "celanapanjang", "casual", "jeans"));
        typePria.put("atasan", Arrays.asList("kaoslenganpendek", "kaoslenganpanjang", "kemeja", "polo"));

        Map<String, List<String>> typeWanita = new HashMap<>();
        typeWanita.put("aksesoris", Arrays.asList("ikatpinggang", "jam", "kacamata", "topi"));
        typeWanita.put("sepatu", Arrays.asList("boots", "heels&wedges", "sendal", "slipon", "sneakers", "olahraga"));
        typeWanita.put("luaran", Arrays.asList("cardigan", "crewneck", "hoodie", "jaket", "jas", "mantel", "sweater"));
        typeWanita.put("bawahan", Arrays.asList("celanapendek", "celanapanjang", "rokpendek&3/4", "rokpanjang"));
        typeWanita.put("atasan", Arrays.asList("kaoslenganpendek&3/4", "kaoslenganpanjang", "kemejadanblus", "gaun"));

        Map<String, List<String>> typeAnak = new HashMap<>();
        typeAnak.put("aksesoris", Arrays.asList("kacamata", "topi"));
        typeAnak.put("sepatu", Arrays.asList("boots", "formal", "heels&wedges", "sendal", "slipon", "sneakers", "olahraga"));
        typeAnak.put("luaran", Arrays.asList("cardigan", "crewneck", "hoodie", "jaket", "jas", "mantel", "sweater"));
        typeAnak.put("bawahan", Arrays.asList("celanapendek", "celanapanjang", "rokpendek&3/4", "rokpanjang"));
        typeAnak.put("atasan", Arrays.asList("kaoslenganpendek", "kaoslenganpanjang", "kemejadanblus", "gaun", "polo"));

        String category = faker.options().option("pria", "wanita", "anak");
        String subcategory = faker.options().option("atasan", "bawahan", "luaran", "sepatu", "aksesoris");
        String type = "";
        List<String> data;
        switch (category) {
          case "pria":
            data = typePria.get(subcategory);
            type = data.get(faker.number().numberBetween(0, data.size()-1));
            break;
          case "wanita":
            data = typeWanita.get(subcategory);
            type = data.get(faker.number().numberBetween(0, data.size()-1));
            break;
          case "anak":
          default:
            data = typeAnak.get(subcategory);
            type = data.get(faker.number().numberBetween(0, data.size()-1));
        }

        product.setCategory(category);
        product.setSubcategory1(subcategory);
        product.setSubcategory2(type);
        product.setStore(store);
        products.add(product);
      }
      productRepository.saveAll(products);
    }
  }

  private void seedStore() {
    long u = storeRepository.count();
    if(u > 0) return;
    Random rand = new Random();
    List<User> user = userRepository.findByRole("ROLE_SELLER");
    List<Store> stores = new ArrayList<>();
    for (User value : user) {
        Store store = new Store();
        store.setId(UUID.randomUUID());
        String companyName = faker.company().name();
        store.setName(companyName);
        store.setUser(value);
        store.setProvince(faker.options().option("Banten", "DKI Jakarta", "Kalimantan Tengah", "Nanggroe Aceh Darussalam (NAD)"));
        String province = store.getProvince();
        switch (province) {
            case "Banten":
                store.setCity(faker.options().option("Cilegon", "Lebak", "Pandeglang", "Serang"));
                break;
            case "DKI Jakarta": 
                store.setCity(faker.options().option("Jakarta Barat", "Jakarta Pusat", "Jakarta Selatan", "Jakarta Timur"));
                break;
            case "Kalimantan Tengah":
                store.setCity(faker.options().option("Barito Selatan", "Barito Timur", "Barito Utara", "Gunung Mas"));
                break;
            case "Nanggroe Aceh Darussalam (NAD)":
                store.setCity(faker.options().option("Aceh Barat", "Aceh Besar", "Aceh Jaya", "Aceh Singkil"));
                break;
        }
        store.setAbout(faker.lorem().paragraph());
        store.setBanner("https://thrifthouse.herokuapp.com/images/storedefault/defaultbanner.png");
        store.setPhoto("https://thrifthouse.herokuapp.com/images/storedefault/defaultlogo.png");
        store.setLogo("https://thrifthouse.herokuapp.com/images/storedefault/defaultlogo.png");
        store.setDeliveryService(faker.options().option("jne", "pos", "tiki"));
        store.setBankHolder(companyName);
        store.setBankName(faker.options().option("Bank BCA", "Bank BNI", "Bank BRI", "Bank Mandiri"));
        store.setBankNumber(String.format("%d %d %d", rand.nextInt(900) + 100, rand.nextInt(900) + 100, rand.nextInt(9000) + 1000));
        stores.add(store);
    }
    storeRepository.saveAll(stores);
  }

  private void seedUsers() {
    long u = userRepository.count();
    if(u > 0) return;
    List<User> users = new ArrayList<>();

    User defaultUser = new User();
    defaultUser.setUsername("thriftUser");
    defaultUser.setPassword("$2a$12$SNMX6RvqFKMrAXjInyb7Q.1ZAiRBSHtnWlHqLMvxc049YSatpkDJy");
    defaultUser.setEmail("downloadmeinhere@gmail.com");
    defaultUser.setPhone("081234567890");
    defaultUser.setRole(roles[1]);
    defaultUser.setProfileImg("https://picsum.photos/480/480");
    userRepository.save(defaultUser);

    User defaultSeller = new User();
    defaultSeller.setUsername("thriftSeller");
    defaultSeller.setPassword("$2a$12$SNMX6RvqFKMrAXjInyb7Q.1ZAiRBSHtnWlHqLMvxc049YSatpkDJy");
    defaultSeller.setEmail("sellerthrifthouse@gmail.com");
    defaultSeller.setPhone("081234567890");
    defaultSeller.setRole(roles[2]);
    defaultSeller.setProfileImg("https://picsum.photos/480/480");
    userRepository.save(defaultSeller);

    User defaultAdmin = new User();
    defaultAdmin.setUsername("thriftAdmin");
    defaultAdmin.setPassword("$2a$12$SNMX6RvqFKMrAXjInyb7Q.1ZAiRBSHtnWlHqLMvxc049YSatpkDJy");
    defaultAdmin.setEmail("appthrifthouse@gmail.com");
    defaultAdmin.setPhone("081234567890");
    defaultAdmin.setRole(roles[0]);
    defaultAdmin.setProfileImg("https://picsum.photos/480/480");
    userRepository.save(defaultAdmin);

    for (int i = 0; i < 100; i++) {
      User user = new User();
      user.setId(UUID.randomUUID());
      user.setUsername(faker.name().username());
      user.setPassword("$2a$12$SNMX6RvqFKMrAXjInyb7Q.1ZAiRBSHtnWlHqLMvxc049YSatpkDJy");
      user.setEmail(faker.internet().emailAddress());
      user.setPhone(faker.phoneNumber().phoneNumber());
      user.setRole(faker.options().nextElement(roles));
      user.setProfileImg("https://picsum.photos/480/480");;
      users.add(user);
    }
    userRepository.saveAll(users);
  }

  private void seedAddress() {
    long u = addressRepository.count();
    if(u > 0) return;
    List<Address> addressList = new ArrayList<>();

    // User
    //Surabaya
    String[] nmSby          = {"Mas Ari", "Budhe Arga", "Mas Yoram"};
    String[] noHpSby        = {"085733350494", "085712340494", "085733350666"};
    int idProvSby           = 11;
    String provSby          = "Jawa Timur";
    int idCitySby           = 444;
    String citySby          = "Surabaya";
    String[] districtSby    = {"Sukomanunggal", "Gn. Anyar", "Kenjeran"};
    String[] villageSby     = {"Tanjungsari", "Rungkut Tengah", "Tanah Kali Kedinding"};
    String[] fullAddressSby = {"Tanjungsari jaya bhakti no:38", "Jl. Rungkut Mapan Utara", "Jl. Kedung Cowek No.373"};
    String[] postCodeSby    = {"60187", "60293", "60129"};
    String[] detailSby      = {"Rumah Utara rel Kereta Api", "Samsat Corner Rungkut Yakaya (Samsat Corner Yakaya Rungkut Mapan)", ""};

   // Bekasi
    String[] nmBks          = {"Mas Faizal", "Mas Daniel"};
    String[] noHpBks        = {"08268732670", "085712340494"};
    int idProvBks           = 9;
    String provBks          = "Jawa Barat";
    int idCityBks           = 55;
    String cityBks          = "Bekasi";
    String[] districtBks    = {"Tambun Selatan", "Pejuang"};
    String[] villageBks     = {"Jatimulya", "Medan Satria"};
    String[] fullAddressBks = {"Jl. Palem 1 No 4 RT 04 RW 08", "Permata Harapan Baru B2/24, RT 004/ RW 014"};
    String[] postCodeBks    = {"17510", "17131"};
    String[] detailBks      = {"", ""};

    // Surakarta
    String[] nmSrt          = {"Mas Farhan"};
    String[] noHpSrt        = {"08268732670"};
    int idProvSrt           = 10;
    String provSrt          = "Jawa Tengah";
    int idCitySrt           = 445;
    String citySrt          = "Surakarta (Solo)";
    String[] districtSrt    = {"Pasar Kliwon"};
    String[] villageSrt     = {"Kampung Baru"};
    String[] fullAddressSrt = {"Jl. Ronggowarsito No.24c RT 002 RW 003"};
    String[] postCodeSrt    = {"57111"};
    String[] detailSrt      = {""};

    List<User> roleUser = userRepository.findByRole("ROLE_USER");
    int countI = 0;
    for (User user: roleUser) {
      if(countI % 2 == 0){
        for (int iSby = 0; iSby < nmSby.length ; iSby++) {
          addressList.add(setAddress(user, nmSby[iSby], noHpSby[iSby],  iSby == 0, idProvSby,
                  provSby, idCitySby, citySby, districtSby[iSby], villageSby[iSby], fullAddressSby[iSby], postCodeSby[iSby], detailSby[iSby]
          ));
        }
      }else if(countI % 3 == 0){
        for (int iBks = 0; iBks < nmBks.length ; iBks++) {
          addressList.add(setAddress(user, nmBks[iBks], noHpBks[iBks],  false, idProvSrt,
                  provBks, idCityBks, cityBks, districtBks[iBks], villageBks[iBks], fullAddressBks[iBks], postCodeBks[iBks], detailBks[iBks]
          ));
        }
      }else{
        for (int iSrt = 0; iSrt < nmSrt.length ; iSrt++) {
          addressList.add(setAddress(user, nmSrt[iSrt], noHpSrt[iSrt],  true, idProvSby,
                  provSrt, idCitySrt, citySrt, districtSrt[iSrt], villageSrt[iSrt], fullAddressSrt[iSrt], postCodeSrt[iSrt], detailSrt[iSrt]
          ));
        }
      }
      countI++;
    }

    // Seller And Admin
    // Jakarta
    String[] nmJkt          = {"Mas Rangga"};
    String[] noHpJkt        = {"(021) 62313000"};
    int idProvJkt           = 6;
    String provJkt          = "DKI Jakarta";
    int idCityJkt           = 155;
    String cityJkt          = "Jakarta Utara";
    String[] districtJkt    = {"Pademangan"};
    String[] villageJkt     = {"Pademangan Barat"};
    String[] fullAddressJkt = {"Jl. Gunung Sahari Raya No.1"};
    String[] postCodeJkt    = {"14420"};
    String[] detailJkt      = {""};

    List<User> roleSeller   = userRepository.findAll();
    for (User seller : roleSeller) {
        if(!seller.getRole().equals("ROLE_USER")){
          for (int iJkt = 0; iJkt < nmJkt.length ; iJkt++) {
            addressList.add(setAddress(seller, nmJkt[iJkt], noHpJkt[iJkt],  false, idProvJkt,
                    provJkt, idCityJkt, cityJkt, districtJkt[iJkt], villageJkt[iJkt], fullAddressJkt[iJkt], postCodeJkt[iJkt], detailJkt[iJkt]
            ));
          }
        }
    }
    addressRepository.saveAll(addressList);
  }

  private Address setAddress(User userId, String name, String noHp, boolean addressLabel, int idProv,
                             String prov, int idCity, String city, String district, String village,
                            String fullAddress, String postCode, String detail){
    Address address = new Address();
    address.setUser(userId);
    address.setId(UUID.randomUUID());
    address.setRecipientName(name);
    address.setRecipientPhone(noHp);
    address.setAddressLabel(addressLabel);
    address.setIdProvince(idProv);
    address.setProvince(prov);
    address.setIdCity(idCity);
    address.setCity(city);
    address.setDistrict(district);
    address.setVillage(village);
    address.setFullAddress(fullAddress);
    address.setPostalCode(postCode);
    address.setDetail(detail);
    return address;
  }

  private void seedFavoriteProducts() {
    String sql = "SELECT count(U.*) FROM favorite_product U";
    Integer u = jdbcTemplate.queryForObject(sql, Integer.class);
    if (u != null) {
      if(u > 0) return;
    }
    List<User> users = userRepository.findAll();
    List<Product> products = productRepository.findAll();

    // Seeder Ari
    // int i = 0;
    // for (User user: users) {
    //   List<Product> favorites = new ArrayList<>();
    //   favorites.add(products.get(i));
    //   user.setFavoriteProducts(favorites);
    //   userRepository.save(user);
    //   i++;
    // }

    Random rand;
    for (User user: users) {
        rand = new Random();
        List<FavoriteProduct> favorites = new ArrayList<>();
        int num1 = rand.nextInt(10) + 1;
        List<Product> selectedProducts = new ArrayList<>();
        for (int i = 0; i < num1; i++) {
            Product product = products.get(rand.nextInt(products.size()));

            if (!selectedProducts.contains(product)) {
                selectedProducts.add(product);
            }
        }

        for (Product product : selectedProducts) {
            FavoriteProduct favProduct = new FavoriteProduct(
                new FavoriteProductKey(user.getId(), product.getId()),
                user,
                product
            );
            this.favoriteProductRepository.save(favProduct);
            favorites.add(favProduct);
        }
        user.setFavoriteProducts(favorites);
        userRepository.save(user);
    }

  }

  private void seedBank() {
    long u = this.bankRepository.count();
    if(u > 0) return;

    Random rand = new Random();

    String[] bankNames = {"Bank BCA", "Bank BNI", "Bank BRI", "Bank Mandiri"};
    String[] bankLogos = {"/banklogo/logobca.png", "/banklogo/logobni.png", "/banklogo/logobri.png", "/banklogo/logomandiri.png"};
    String BASE_URL = "https://thrifthouse.herokuapp.com/images";
    for(int i = 0; i < bankNames.length; i++) {
      Bank bank = new Bank(
        null,
        bankNames[i],
        String.format("%d %d %d", rand.nextInt(900) + 100, rand.nextInt(900) + 100, rand.nextInt(9000) + 1000),
        "a/n PT. Thrifhouse",
        BASE_URL + bankLogos[i]
      );
      this.bankRepository.save(bank);
    }

  }

  private void seedOrder() {
    long u = this.orderRepository.count();
    if(u > 0) return;

    Random rand = new Random();
    
    List<User> users = this.userRepository.findByRole("ROLE_USER");
    List<Product> products = this.productRepository.findAll();

    int userSize = users.size();
    String[] statuses = {"Menunggu pembayaran", "Menunggu pembayaran", "Pesanan ditolak", "Pesanan dibatalkan", "Pembayaran terkonfirmasi", "Pesanan diproses", "Pesanan diproses", "Selesai"};
    for (User user : users) {
      for (int i = 0; i < statuses.length; i++) {
        String orderCode = "TH-" + String.valueOf(System.currentTimeMillis());
        Bank bank = this.bankRepository.findAll().get(rand.nextInt(4));
        List<String> orderedProducts = new ArrayList<>();

        for (int j = 0; j < rand.nextInt(10); j++) {
          orderedProducts.add(products.get(rand.nextInt(products.size())).getId().toString());
        }

        Address address = this.addressRepository.findAll().stream()
                .filter(adr -> adr.getUser().getId().compareTo(user.getId()) == 0)
                .collect(Collectors.toList()).get(0);

        Order order = new Order(
                null,
                orderCode,
                orderedProducts.toString(),
                faker.options().option("jne", "tiki", "pos"),
                rand.nextInt(100000 + 1),
                "shipping-services",
                "etd",
                rand.nextInt(100000 + 1),
                address,
                user,
                bank
        );

        String status = statuses[i];
        String packagingStatus = "";
        boolean isRejected = false;
        String receipt = "";

        switch (status) {
          case "Menunggu pembayaran":
            receipt = i > 0 ? "https://picsum.photos/480/480" : "";
            break;
          case "Pesanan ditolak":
            receipt = "https://picsum.photos/480/480";
          case "Pesanan dibatalkan":
            isRejected = true;
            break;
          case "Pesanan diproses":
            receipt = "https://picsum.photos/480/480";
            packagingStatus = i > 5 ? "Pesanan dikemas" : "Pesanan dikirim";
            break;
          case "Selesai":
            receipt = "https://picsum.photos/480/480";
            packagingStatus = "Pesanan diterima";
            break;
        }

        order.setStatus(status);
        order.setPackagingStatus(packagingStatus);
        order.setIsRejected(isRejected);
        order.setReceipt(receipt);

        this.orderRepository.save(order);
      }
    }
  }

}
