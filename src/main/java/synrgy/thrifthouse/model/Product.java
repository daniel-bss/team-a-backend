package synrgy.thrifthouse.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import synrgy.thrifthouse.repository.FilterProduct.FilterProductRepository;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product")
@DynamicUpdate
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "store_id")
    @FilterProductRepository.Filtered(joinField = "name")
    private Store store;

    @Column(name = "name")
    private String name;

    @Column(name = "brand")
    private String brand;

    @Column(name = "size")
    private String size;

    @Column(name = "condition")
    private String condition;

    @Column(name = "price")
    private Integer price;

    @Column(name = "height")
    private Integer height;

    @Column(name = "width")
    private Integer width;

    @Column(name = "material")
    private String material;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Lob
    @Column(name = "photos")
    @Getter(lombok.AccessLevel.NONE)
    private String photos; // "{1:'/url'. 2:'url', 3:'/url'. 4:'url'}" or "["/url", "/url", "/url"]"

    @Column(name = "category")
    private String category; // "pria", "wanita", "anak"

    @Column(name = "subcategory1")
    private String subcategory1; // "aksesoris", "sepatu", "luaran", "bawahan", "atasan"

    @Column(name = "subcategory2")
    private String subcategory2;

    @Column(name = "sold", columnDefinition = "boolean default false")
    private boolean sold;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP", name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    public void setPhotos(List<String> photos) {
        this.photos = photos!=null ? photos.toString() : null;
    }

    public List<String> getPhotos() {
        if (this.photos!=null) {
            return new ArrayList<String>(
                    java.util.Arrays.asList(
                            this.photos.substring(1, this.photos.length() - 1).split(", ")));
        } else {
            return null;
        }
    }

    @OneToMany(mappedBy = "product")
    private Collection<FavoriteProduct> favoriteUser;

}
