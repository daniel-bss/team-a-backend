package synrgy.thrifthouse.model;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "\"order\"")
public class Order {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "order_code")
    private String orderCode;
    
    @Lob
    @Column(name = "product_ids")
    private String productIds;
    
    @Column(name = "delivery_service")
    private String deliveryService;

    @Column(name = "shipping_cost")
    private Integer shippingCost;

    @Column(name = "shipping_service")
    private String shippingService;

    @Column(name = "etd")
    private String estimatedTimeOfDeparture;
    
    @Column(name = "products_price")
    private Integer productsPrice;
    
    @Column(name = "status")
    private String status = "Menunggu pembayaran";
    
    @Lob
    @Column(name = "receipt")
    private String receipt = "";

    @Column(name = "expired_at")
    private Date expiredAt = new Date(System.currentTimeMillis() + 86400000L);

    @Column(name = "is_rejected")
    private Boolean isRejected = false;

    @Column(name = "packaging_status")
    private String packagingStatus = "";

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    public Order(UUID id, String orderCode, String productIds, String deliveryService, 
    Integer shippingCost, String shippingService, String estimatedTimeOfDeparture, Integer productsPrice, 
    Address address, User user, Bank bank) {
        this.id = id;
        this.orderCode = orderCode;
        this.productIds = productIds;
        this.deliveryService = deliveryService;
        this.shippingCost = shippingCost;
        this.shippingService = shippingService;
        this.estimatedTimeOfDeparture = estimatedTimeOfDeparture;
        this.productsPrice = productsPrice;
        this.address = address;
        this.user = user;
        this.bank = bank;
    }

    public List<UUID> getListProductIds() {
        if (this.productIds!=null) {
            if (this.productIds.equals("[]")) {
                return new ArrayList<>();
            } else {
                return Arrays.stream(
                                this.productIds.substring(1, this.productIds.length() - 1).split(", "))
                        .map(UUID::fromString).collect(Collectors.toList());
            }
        } else {
            return null;
        }
    }
    
}
