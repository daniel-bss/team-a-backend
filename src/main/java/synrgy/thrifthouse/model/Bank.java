package synrgy.thrifthouse.model;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bank")
public class Bank {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @OneToMany(mappedBy = "bank")
    private Collection<Order> orders;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "bank_number")
    private String bankNumber;

    @Column(name = "bank_holder")
    private String bankHolder;

    @Lob
    @Column(name = "bank_logo")
    private String bankLogo;

    public Bank(UUID id, String bankName, String bankNumber, String bankHolder, String bankLogo) {
        this.id = id;
        this.bankName = bankName;
        this.bankNumber = bankNumber;
        this.bankHolder = bankHolder;
        this.bankLogo = bankLogo;
    }

}
