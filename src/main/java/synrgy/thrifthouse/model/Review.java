package synrgy.thrifthouse.model;

import javax.persistence.*;

import com.sun.istack.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "review")
public class Review {

    @EmbeddedId
    private ReviewKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @MapsId("storeId")
    @JoinColumn(name = "store_id")
    private Store store;

    @Lob
    @Column(name = "user_ids")
    @Getter(lombok.AccessLevel.NONE)
    @Setter(lombok.AccessLevel.NONE)
    private String userIds;

    @Column(name = "description")
    private String description;

    @Column(name = "rating", columnDefinition = "int default 0")
    private Integer rating = 0;

    @Lob
    @Column(name = "photos")
    @Getter(lombok.AccessLevel.NONE)
    private String photos;

    @Column(name = "is_anonim", columnDefinition = "boolean default false")
    private boolean isAnonim;

    @CreationTimestamp
    @Column(columnDefinition = "TIMESTAMP", name = "created_at", updatable = false)
    private LocalDateTime createdAt;


    // Photos' getter & setter

    public void setPhotos(List<String> photos) {
        this.photos = photos!=null ? photos.toString() : null;
    }

    public List<String> getPhotos() {
        if (this.photos != null){
            return new ArrayList<>(
                    Arrays.asList(
                            this.photos.substring(1, this.photos.length() - 1).split(", ")));
        } else {
            return null;
        }
    }

    public void setUserIds(List<String> photos) {
        this.userIds = photos!=null ? photos.toString() : null;
    }

    // userIds getter & setter

    public List<String> getUserIds() {
        if (this.userIds!=null) {
            return new ArrayList<String>(
                    java.util.Arrays.asList(
                            this.userIds.substring(1, this.userIds.length() - 1).split(", ")));
        } else {
            return null;
        }
    }

    public Review(ReviewKey id, User user, Store store, String description, Integer rating, String photos) {
        this.id = id;
        this.user = user;
        this.store = store;
        this.description = description;
        this.rating = rating;
        this.photos = photos;
    }
}
