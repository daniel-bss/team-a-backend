package synrgy.thrifthouse.mapper;

import java.util.Collection;
import java.util.UUID;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import synrgy.thrifthouse.dto.dashboard.StoreSummaryDto;
import synrgy.thrifthouse.dto.product.FavStoreDto;
import synrgy.thrifthouse.dto.product.StoreInformationDto;
import synrgy.thrifthouse.dto.store.FavoriteStoreDto;
import synrgy.thrifthouse.dto.store.StoreCreatePostDto;
import synrgy.thrifthouse.dto.store.StoreDetailBannerDto;
import synrgy.thrifthouse.dto.store.StoreKtpDto;
import synrgy.thrifthouse.model.FavoriteStore;
import synrgy.thrifthouse.model.Product;
import synrgy.thrifthouse.model.Review;
import synrgy.thrifthouse.model.Store;

@Mapper(componentModel = "spring")
public interface StoreMapper {
  StoreMapper INSTANCE = Mappers.getMapper(StoreMapper.class);

  @Mapping(source = "favorites", target = "favoriteStore", qualifiedByName = "getTotalFavorites")
  @Mapping(source = "reviews", target = "totalReview", qualifiedByName = "getTotalReviews")
  @Mapping(source = "reviews", target = "averageReview", qualifiedByName = "getAverageReview")
  @Mapping(source = "products", target = "totalProduct", qualifiedByName = "getTotalProducts")
  StoreInformationDto StoreToStoreInformationDto(Store store);

  @Mapping(source = "favorites", target = "favoriteStore", qualifiedByName = "getTotalFavorites")
  @Mapping(source = "reviews", target = "totalReview", qualifiedByName = "getTotalReviews")
  @Mapping(source = "reviews", target = "averageReview", qualifiedByName = "getAverageReview")
  @Mapping(source = "products", target = "totalProduct", qualifiedByName = "getTotalProducts")
  FavStoreDto StoreToFavStoreDto(Store store);

  @Mapping(source = "favorites", target = "favoriteStore", qualifiedByName = "getTotalFavorites")
  @Mapping(source = "reviews", target = "averageReview", qualifiedByName = "getAverageReview")
  @Mapping(source = "products", target = "totalProduct", qualifiedByName = "getTotalProducts")
  StoreSummaryDto StoreToStoreSummaryDto(Store store);

  @Named("getTotalFavorites")
  public static int getTotalFavorites(Collection<FavoriteStore> favorites) {
    return favorites.size();
  }

  @Named("getTotalReviews")
  public static int getTotalReviews(Collection<Review> reviews) {
    return reviews.size();
  }

  @Named("getAverageReview")
  public static double getAverageReview(Collection<Review> reviews) {
    double total = 0;
    for (Review review : reviews) {
      total += review.getRating();
    }
    Double result = total / (reviews.size() != 0 ? reviews.size() : 1);
    result = result * 10;
    result = Math.round(result) / 10.0;
    return result;
  }

  @Named("getTotalProducts")
  public static int getTotalProducts(Collection<Product> products) {
    return products.size();
  }

  Store StoreInformationDtoToStore(StoreInformationDto storeInformationDto);

  StoreDetailBannerDto StoreToStoreDetailBannerDto(Store store);

  @Mapping(source = "store", target = "storeId", qualifiedByName = "getFavoriteStoreId")
  FavoriteStoreDto storeToFavoriteStoreDto(Store store);
  @Named("getFavoriteStoreId")
  public static UUID getFavoriteStoreId(Store store) {
    return store.getId();
  }

  @Mapping(target = "ktpImg", ignore = true)
  StoreCreatePostDto StoreToStoreCreatePostDto(Store store);

  @Mapping(target = "ktpImg", ignore = true)
  Store StoreCreatePostDtoToStore(StoreCreatePostDto storeCreatePostDto);

  StoreKtpDto StoreToStoreKtpDto(Store store);
}
