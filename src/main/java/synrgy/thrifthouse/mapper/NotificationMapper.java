package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import synrgy.thrifthouse.dto.notification.NotificationDto;
import synrgy.thrifthouse.model.Notification;
import synrgy.thrifthouse.model.User;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface NotificationMapper {

    @Mapping(source = "notificationAt", target = "notificationAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "readAt", target = "readAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "updatedAt", target = "updatedAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "user", target = "userId", qualifiedByName = "getUserId")
    List<NotificationDto> mapsNotificationsToNotificationDtos(List<Notification> notifications);

    @Mapping(source = "notificationAt", target = "notificationAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "readAt", target = "readAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "updatedAt", target = "updatedAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "user", target = "userId", qualifiedByName = "getUserId")
    NotificationDto NotificationToNotificationDto(Notification notification);

    @Mapping(source = "notificationAt", target = "notificationAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "readAt", target = "readAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "updatedAt", target = "updatedAt", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Notification NotificationDtoToNotification(NotificationDto notificationDto);

    @Named("getUserId")
    public static UUID getUserId(User user) {
        return user.getId();
    }
}
