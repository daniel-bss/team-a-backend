package synrgy.thrifthouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import synrgy.thrifthouse.dto.user.UserDto;
import synrgy.thrifthouse.model.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Mapper(componentModel = "spring")
public interface UserMapper {
    
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    // To DTO
    @Mapping(source = "user", target = "birth", qualifiedByName = "getBirth")
    UserDto userToUserDto(User user);

    @Named("getBirth")
    public static String getBirth(User user) {
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        if(user.getBirth() != null) {
            return df.format(user.getBirth());
        }
        return null;
    }
    
    // To Entity
    @Mapping(source = "password", target = "password", qualifiedByName = "setEncodedPassword")
    @Mapping(source = "profileImg", target = "profileImg", qualifiedByName = "setProfileImg")
    User userDtoToUser(UserDto userDto);

    @Named("setEncodedPassword")
    static String setEncodedPassword(String password) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
    
    @Named("setProfileImg")
    static String setProfileImg(String profileImage) {
        if (profileImage == null || profileImage.length() == 0) { return "https://picsum.photos/480/480"; }
        return profileImage;
    }
    
}
