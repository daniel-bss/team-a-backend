package synrgy.thrifthouse.config;

import com.pusher.rest.Pusher;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PusherConfig {
    private final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    @Bean
    public Pusher getPusher() {
        String appId = dotenv.get("PUSHER_APP_ID", System.getProperty("pusher.appid", ""));
        String key = dotenv.get("PUSHER_KEY", System.getProperty("pusher.key", ""));
        String secret = dotenv.get("PUSHER_SECRET", System.getProperty("pusher.secret", ""));
        
        Pusher pusher = new Pusher(appId, key, secret);
        pusher.setCluster("ap1");
        pusher.setEncrypted(true);
        
        return pusher;
    }
}
